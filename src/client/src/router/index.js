import { createRouter, createWebHistory } from "vue-router";
import publicRoutes from "./public";

const getRoutes = () => {
  return publicRoutes;
};

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: getRoutes(),
});

export default router;
