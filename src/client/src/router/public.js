import HomeView from "../views/HomeView.vue";
import FoundingsView from "../views/public/FoundingsView.vue";
import MinistryView from "../views/public/MinistryView.vue";
import LeadershipView from "../views/public/LeadershipView.vue";
import VisionView from "../views/public/VisionView.vue";
import FaithView from "../views/public/FaithView.vue";
import SundayView from "../views/public/SundayView.vue";
import GroupsView from "../views/public/GroupsView.vue";
import ChurchesView from "../views/public/ChurchesView.vue";
import MinistriesView from "../views/public/MinistriesView.vue";
import EventsView from "../views/public/EventsView.vue";
import ReadingView from "../views/public/ReadingView.vue";

const routes = [
  {
    path: "/",
    name: "home",
    component: HomeView,
  },
  {
    path: "/foundings",
    name: "foundings",
    component: FoundingsView,
  },
  {
    path: "/leadership",
    name: "leadership",
    component: LeadershipView,
  },
  {
    path: "/ministry",
    name: "ministry",
    component: MinistryView,
  },
  {
    path: "/vision",
    name: "vision",
    component: VisionView,
  },
  {
    path: "/faith",
    name: "faith",
    component: FaithView,
  },
  {
    path: "/sunday",
    name: "sunday",
    component: SundayView,
  },
  {
    path: "/groups",
    name: "groups",
    component: GroupsView,
  },
  {
    path: "/churches",
    name: "churches",
    component: ChurchesView,
  },
  {
    path: "/ministries",
    name: "ministries",
    component: MinistriesView,
  },
  {
    path: "/events",
    name: "events",
    component: EventsView,
  },
  {
    path: "/reading",
    name: "reading",
    component: ReadingView,
  },
];

export default routes;
